﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Representing one of the eight directions in which things travel.
/// </summary>
public class Direction
{
    /// <summary>
    /// Set in the constructor.  One of the  eight cardinal directions that things move in.
    /// </summary>
    private EDirection thisDirection;
    /// <summary>
    /// The Vector3 equivilent of the direction.
    /// IE:  North = new Vector3(0, 0, 1);
    /// </summary>
    private Vector3 vector3;

    /// <summary>
    /// Public Read-Only Access on this.
    /// </summary>
    public Vector3 MyVector
    {
        get { return vector3; }
    }

    /// <summary>
    /// Public Read-Only Access for this member.
    /// </summary>
    public EDirection ThisDirection
    {
        get { return thisDirection; }
    }

    public Direction(EDirection thisDirection)
    {
        this.thisDirection = thisDirection;
        switch (thisDirection)
        {
            case EDirection.North:
                vector3 = new Vector3(0, 0, 1);
                break;
            case EDirection.NorthWest:
                vector3 = new Vector3(-1, 0, 1);
                break;
            case EDirection.NorthEast:
                vector3 = new Vector3(1, 0, 1);
                break;
            case EDirection.South:
                vector3 = new Vector3(0, 0, -1);
                break;
            case EDirection.SouthWest:
                vector3 = new Vector3(-1, 0, 0);
                break;
            case EDirection.SouthEast:
                vector3 = new Vector3(1, 0, -1);
                break;
            case EDirection.East:
                vector3 = new Vector3(1, 0, 0);
                break;
            case EDirection.West:
                vector3 = new Vector3(-1, 0, 0);
                break;
            default:
                //Essentially need to get an int from 0 to 5 inclusive.
                throw new System.ArgumentOutOfRangeException();
        }
    }

    /// <summary>
    /// Picks a random direction, from the 8 available in EDirection.
    /// When you want just a RANDOM direction, rather than a specific one, use this method to fetch one for you.
    /// </summary>
    public static Direction GenerateRandomDirection()
    {
        System.Random seed = new System.Random();
        int randomInt = seed.Next(0, 7);
        Direction direction = new Direction((EDirection)randomInt);

        return direction;
    }

}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnitCommand;

/// <summary>
/// What are the public methods on "Cultist":
///     EnterGathering();
/// </summary>
public class Unit : MonoBehaviour
{
    /// <summary>
    /// The queue of commands.  
    /// CommandBase is a superclass that specific commands derive from.
    /// You will need to call the Execute() method on it, every frame / every Update().
    /// </summary>
    private Queue<CommandBase> commandQueue;

    /// <summary>
    /// The command that this unit is currently working.
    /// </summary>
    private CommandBase currentCommand;

    /// <summary>
    /// Reference to the NavMeshAgent for this unit.  
    /// You can also use GetComponent to get the NavMeshAgent; 
    /// I assume doing that once in the Awake() is more efficient.
    /// </summary>
    private NavMeshAgent myNavMeshAgent;

    /// <summary>
    /// The 
    /// </summary>
    private EUnitStatus myStatus = EUnitStatus.Resting;

    /// <summary>
    /// Only use this for setting via the editor.
    /// I really hate how Unity deals with this.
    /// </summary>
    public float SightRange = 10;

    /// <summary>
    /// This is set in Awake().  Defaults to the position that the unit was at
    /// when the game started.
    /// </summary>
    Vector3 _homePoint;

    private MeshRenderer selectedIcon;
    private bool isSelected;
    /// <summary>
    /// The value of this property dictates the behaviour of the UI elements, 
    /// dealing with unit selection.
    /// </summary>
    public bool IsSelected
    {
        get { return isSelected; }
        set 
        { 
            isSelected = value;

            //This enables and disables the renderer for this icon, thus hiding it or making it appear.
            selectedIcon.enabled = value;
        }
    }

    public ETeamType MyTeam { get; private set; }

    /// <summary>
    /// This represents the collection of "things" that this cultist is holding.
    /// </summary>
    private Dictionary<string, int> belongings = new Dictionary<string, int>();

    /// <summary>
    /// Called at scene start, before the first frame even.
    /// Use this to initialize as much as possible.
    /// </summary>
    private void Awake()
    {        
        //IF THIS CAUSES A CRASH, YOU MAY WANT TO MOVE THE FOLLOWING TO THE Start() method.
        //Set the home point to where the gameobject is during the start of the scene.
        _homePoint = gameObject.transform.position;

        //Initialize the value of selectedIcon.
        selectedIcon = gameObject.GetComponentInChildren<MeshRenderer>();
        
        //Need to initalize the command queue data structure.
        commandQueue = new Queue<CommandBase>();
    }

    // Start is called before the first frame update
    void Start()
    {
        myNavMeshAgent = gameObject.GetComponent<NavMeshAgent>();
        if (!myNavMeshAgent)
        {
            Debug.LogError("This cultist doesn't have a NavMeshAgent, and he really should.", gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {   
        //If there is no currentCommand, AND there are commands in the queue (waiting)...
        if (currentCommand == null && commandQueue.Count > 0)
        {
            //Then make the command at the head of the queue the currentCommand.
            currentCommand = commandQueue.Dequeue();
        }

        if (currentCommand != null)
        {
            currentCommand.Execute();
        }
    }

    /// <summary>
    /// NOT IN USE AS OF 11-17 (but check for references before removing).  Keeping for now.
    /// </summary>
    /// <param name="range"></param>
    /// <returns></returns>
    private Unit[] FindOthersByRange(float range)
    {
        Collider[] theOthers = Physics.OverlapSphere(gameObject.transform.position, SightRange);
        List<Unit> cultists = new List<Unit>();

        foreach (Collider item in theOthers)
        {
            if (item.gameObject.GetComponent<Unit>() is Unit)
            {
                cultists.Add(item.gameObject.GetComponent<Unit>());
            }
        }

        return cultists.ToArray();
    }

    /// <summary>
    /// I don't know if I need this quite yet.  This is like saying "Go back home as soon as possible".
    /// This is a public method, unlike ReturnToHome() above.
    /// I thought it important to differentiate between public and private methods...
    /// I don't want the private method ReturnToHome() to behave the same as a private one.
    /// Why?  The private method should be more authoriative than the public one.
    /// The private method just GOES.
    /// </summary>
    private void QueueReturnToHome()
    {
        //Yeah, I know.
        //This one is public though.
        //So if I want the public one to be different, it's like, really easy to change that later.
        ReturnToHome();
    }

    private bool DeclareAllegiance(ETeamType selectedTeam)
    {
        try
        {
            //Run the "Declared a team for the first time" script.
            Debug.Log("I swear my life to you, mighty " + selectedTeam.ToString());
            //IT WOULD BE COOL IF THERE WERE SOME KIND OF... I DON'T KNOW... VISUAL INDICATOR
            //FOR THIS THING HERE.
            MyTeam = selectedTeam;
            return true;
        }
        catch (System.ArgumentOutOfRangeException e)
        {
            Debug.LogException(e);
            Debug.Log("YA DONE FUCKED UP GOOD THIS TIME, BOI!");
            return false;
        }
        
    }

    /// <summary>
    /// This should be a command.
    /// Returns the cultist to their home.  
    /// </summary>
    public void ReturnToHome()
    {
        Debug.Log("Returning to home.  Who?  ", this.gameObject);
        myNavMeshAgent.SetDestination(_homePoint);
    }

    /// <summary>
    /// THIS LETS UNITS RECEIVE "COMMANDS" FROM EXTERNAL SOURCES.
    /// This is going to be the only public method that the controller interacts with.
    /// This is the method that allows the Cultist to receive orders.
    /// </summary>
    /// <returns>A bool value say if or if not this is possible AT THE TIME COMMAND WAS GIVEN.</returns>
    public bool ReceiveCommand(ECommand commandType, params object[] parameters)
    {
        switch (commandType)
        {
            case ECommand.Attack:
                throw new System.NotImplementedException();
            case ECommand.Gather:
                CommandGather commandGather = new CommandGather(gameObject);
                commandQueue.Enqueue(commandGather);
                break;
            case ECommand.GoHome:
                throw new System.NotImplementedException();
                
            default:
                throw new System.ArgumentOutOfRangeException();
        }

        return true;
    }

}

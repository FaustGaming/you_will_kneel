﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnitCommand
{
    class CommandGather : CommandBase
    {
        /// <summary>
        /// The time that the command started running.
        /// Set during the Starup phase.
        /// </summary>
        private float timeStarted;

        /// <summary>
        /// How much time may be spent gathering.
        /// Initialized in the constructor.
        /// </summary>
        private float timeRemaining;

        /// <summary>
        /// The position that this is going to gather at.
        /// Vector3 is a struct, meaning that while uninitialized, it still has a default value (of 0,0,0).
        /// That's why this has to be a nullable Vector3.
        /// </summary>
        private Nullable<Vector3> gatheringPosition;

        private UnityEngine.AI.NavMeshAgent navAgent
        {
            get
            {
                return gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>();
            }
        }

        /// <summary>
        /// Constructor for gathering class.
        /// </summary>
        /// <param name="actionType">The action type of the implementation.</param>
        /// <param name="gameObject">The GameObject (hopefully containing the appropriate script) that this actions deals with.</param>
        public CommandGather(GameObject gameObject, float duration = 10.0f)
        {
            commandType = ECommand.Gather;
            this.gameObject = gameObject;
            status = ECommandStatus.Waiting;
            timeRemaining = duration;
        }

        /// <summary>
        /// Run the order.  Tell the caller the status w/ return value.
        /// For a command with the ECommandStatus of Waiting, it will START it.
        /// !! IMPORTANT !!  This should be called each frame --- each Update() --- as such we use Time.deltaTime (it is frame-rate dependant).
        /// </summary>
        override public void Execute()
        {
            switch (Status)
            {
                case ECommandStatus.Waiting:
                    //A command starts in the status of waiting.
                    //The first Execute() call starts it.  
                    Debug.Log("Starting CommandGather.");
                    status = ECommandStatus.StartUp;
                    break;

                case ECommandStatus.StartUp:
                    //Set timeStarted to the time that the command started to "StartUp"
                    timeStarted = Time.time;

                    //Try to find a location to gather, which is randomly determined by FindLocationToGather().
                    gatheringPosition = FindLocationToGather();

                    //If gatheringPosition (a nullable struct) IS NULL, then we're basically going to "skip" this command.
                    //  (Check to see why FindLocationToGather() can return a nullable Vector3, for more information.)
                    //In this startup phase, we're going to set the status of this command to "done".
                    //That way, during the NEXT FRAME --- the next Update() call on this gameobject --- it'll trigger the "Done" phase.
                    if (!gatheringPosition.HasValue)
                    {
                        status = ECommandStatus.Done;
                        StatusMessage = String.Format("The human named {0} tried, but couldn't find a place to gather.", gameObject.name);
                    }
                    else
                    {
                        //Since the StartUp phase is complete, set the status to Running.
                        status = ECommandStatus.Running;

                        //Get the NavMeshAgent from the gameobject.
                        UnityEngine.AI.NavMeshAgent navMeshAgent = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>();

                        //Tell that agent to go to the gathering location.
                        navMeshAgent.SetDestination(gatheringPosition.Value);
                    }

                    break;

                case ECommandStatus.Running:
                    //If we're running, then subtract the timeRemaining by the time passed (deltaTime; framerate dependant).
                    timeRemaining -= Time.deltaTime;

                    //If we've run out of time...
                    if (timeRemaining <= 0)
                    {
                        //Set the status of this command to "Stopping".
                        status = ECommandStatus.Stopping;
                    }

                    break;
                case ECommandStatus.Stopping:
                    //Send the unit "home".
                    gameObject.GetComponent<Unit>().ReturnToHome();
                    status = ECommandStatus.Done;
                    break;

                case ECommandStatus.Done:
                    //Debug message:  Say that this completed.
                    string debugMessage = String.Format("{0} Finished", gameObject.name);
                    Debug.Log(debugMessage);

                    break;

                case ECommandStatus.Error:
                    throw new Exception();

                default:
                    throw new IndexOutOfRangeException();
            }
        }

        /// <summary>
        /// This method generates a Vector3, that is ideally a valid spot on the terrain!
        /// NavMeshAgent.SetDestination(Vector3) actually returns a bool value, saying if or if not 
        /// it is a valid position.
        /// </summary>
        /// <returns>A NULLABLE Vector3.  Note that the actual Vector3 is boxed within the Nullable object.</returns>
        private Nullable<Vector3> FindLocationToGather()
        {
            //Choose a direction.
            Direction randomlyChosenDirection = Direction.GenerateRandomDirection();

            //Find a spot on the NavMesh that is roughly in that direction.
            //It's almost like I want to throw a rock, and however far I can throw it, is how far I can go in a turn.

            //So, from this cultists' location...
            Vector3 myCurrentPos = this.gameObject.transform.position;

            //Shoot a line that goes out a certain distance, DETERMINED BY SIGHT RANGE.
            //How long is that line?  (Need a magnitude).
            Vector3 change = randomlyChosenDirection.MyVector * gameObject.GetComponent<Unit>().SightRange;

            //This is where it needs to head... roughly.
            Vector3 myDestination = myCurrentPos + Vector3.down;

            //I suppose you need to take this modified position, and shoot a ray down though it to the terrain.
            Vector3 wayAboveTarget = myDestination + new Vector3(0, 100, 0);
            Ray newRay = new Ray(wayAboveTarget, myDestination);
            //That should be a straight line pointing down from where the modified position is.

            //Now you need to do a raycast, using this ray.  
            //You're trying to hit a position on the terrain.
            RaycastHit raycastHit;  //Used to store the output of the raycast.
            bool didWeHit;
            didWeHit = Physics.Raycast(newRay, out raycastHit, 300.0f, LayerMask.GetMask("Terrain"));

            //If we DID hit something, then... that's good news!  You can go there!  (I think?)
            if (didWeHit)
            {
                Vector3 pointOnTerrain = raycastHit.point;

                navAgent.SetDestination(pointOnTerrain);
                //Tell the NavMeshAgent to go there.

                //WHAT HAPPENS NEXT?
                Debug.Log("Going this location to gather!  Location:  " + pointOnTerrain.ToString());
                return pointOnTerrain;
            }
            else
            {
                Debug.LogError("This cultist's procedure to find a spot to gather failed.", gameObject);
                return null;
            }
        }
    } //End of class definition.
}//End of namespace.

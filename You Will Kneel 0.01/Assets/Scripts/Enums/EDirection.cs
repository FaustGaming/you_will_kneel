﻿using UnityEngine;
using System.Collections;

public enum EDirection
{
    NorthWest,
    North,
    NorthEast,
    SouthWest,
    South,
    SouthEast,
    East,
    West
}
﻿/// <summary>
/// The various types of teams that a living thing can align themselves with.
/// </summary>
public enum ETeamType
{
    Player,
    RivalGod,
    FriendlyGod,
    Humans
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnitCommand
{
    /// <summary>
    /// Abstract class for describing and executing all "Unit Commands".
    /// Using the word "command", instead of "Action", 
    /// because "Action" is a System class that this system uses.
    /// </summary>
    abstract class CommandBase
    {
        /// <summary>
        /// What type of command is this?
        /// </summary>
        protected ECommand commandType;
        /// <summary>
        /// The gameObject that this Action "works on".
        /// </summary>
        protected GameObject gameObject;

        protected ECommandStatus status;
        /// <summary>
        /// Public getter method for status.
        /// </summary>
        public ECommandStatus Status
        {
            get { return status; }
        }

        /// <summary>
        /// A status message; it's just a string.  
        /// </summary>
        public string StatusMessage { get; protected set; }

        /// <summary>
        /// Evaluate and process this command and its status for each frame, using Execute() 
        /// </summary>
        abstract public void Execute();
    }
}

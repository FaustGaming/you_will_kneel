﻿/// <summary>
/// A static place to declare the delegate types in use, by namespace.
/// </summary>
namespace Delegates
{
    public delegate bool Order();
}
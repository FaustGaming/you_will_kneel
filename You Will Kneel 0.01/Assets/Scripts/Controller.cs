﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    /// <summary>
    /// This is a multiplier of the force that is applied to the rigidbody.
    /// The force is derived from the Input.GetAxis() calls that happen in ReadMovement();
    /// </summary>
    public float MovementMultiple;
    /// <summary>
    /// How much do we multiply Input.GetAxis("MouseX" / "MouseY") by, in order to get the angle of change 
    /// per frame?
    /// </summary>
    public float CameraLookMultiple;
    /// <summary>
    /// The rigidbody attached to this gameobject.  
    /// Yeah, we're using forces for basic movment, you mad man.
    /// CONVENTION NOTE:  We're going to always avoid using the DEPRECIATED properties that "automatically"
    /// find components like a camera or rigidbody.  (The only exception being the "transform", since everything MUST have a transform.)
    /// </summary>
    private new Rigidbody rigidbody;

    /// <summary>
    /// The camera attached to this object.  There HAS to be one.
    /// </summary>
    private new Camera camera;

    /// <summary>
    /// If there is any, then this holds a reference to the object that we've last selected.
    /// Remember to null this out when you mean to deselect a current selection.
    /// </summary>
    private GameObject _selectedObject;
    public GameObject SelectedObject
    {
        private set
        {
            //If there is an object already selected...
            if (_selectedObject != null)
            {
                Debug.Log("Deselecting the previously selected: ", _selectedObject);

                //We haven't updated any values yet, with value.
                //Let's see if we HAD a unit selected, previously.
                Unit oldSelection = value.GetComponent<Unit>();
                if (oldSelection != null)
                {
                    //If so, then tell it, that it isn't selected anymore.  (It will deal with it.)
                    oldSelection.IsSelected = value;
                }
                else 
                { 
                    Debug.Log("No Unit script on this gameobject to deselect.");  
                }

                //Delete the existing reference.
                _selectedObject = null;
            }

            _selectedObject = value;
        }
        get
        {
            return _selectedObject;
        }
    }

    private Avatar avatar;

    /// <summary>
    /// For Cultist, just checks to make sure all editor values have been set correctly.
    /// </summary>
    void Awake()
    {
        //Run through some checks to make sure all the proper values are being set.
        if (MovementMultiple == 0)
        {
            Debug.LogError("MovementMultiple not set, will make moving difficult.  Check controller script.", gameObject);
        }
        if (CameraLookMultiple == 0)
        {
            Debug.LogError("CameraLookMultiple not set, will make looking difficult.  Check controller script.", gameObject);
        }

        //If this throws and error, then you don't  have a camera attached to this object (you silly bastard).
        camera = gameObject.GetComponentInChildren<Camera>();
        if (!camera)
        {
            Debug.LogError("Could not find camera on this gameObject.  One is required.", gameObject);
        }

        avatar = gameObject.GetComponent<Avatar>();
        if (!avatar)
        {
            Debug.LogError("The controller script could not find the avatar script.  That's gonna be a problem.");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
    
    }

    // Update is called once per frame
    /// <summary>
    /// Controller.Update() calls...
    ///     1.)  ReadInput()
    ///             The purpose of this is to process all user input.  
    ///             This is where PLAYER BEHAVIOUR enters the game, so to speak.
    ///             So if you're looking for that, well... start here?
    /// </summary>
    void Update()
    {
        ReadInput();
    }

    /// <summary>
    /// This handles the keypress that deals with moving the player avatar.
    /// Starting from where the mouse cursor currently is, draw a ray from the camera through that point. 
    /// If it hits the terrain, then tell the NavMeshAgent for the avatar to go there.
    /// </summary>
    private void ReadMovement()
    {
        //Draw a ray from the mouse point, to the terrain.
        bool didWeHit;
        Ray mouseRay = camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit raycastHit;
        float raycastRange = 300.0f;
        int layersToCheck = LayerMask.GetMask("TerrainMain");
        didWeHit = Physics.Raycast(mouseRay, out raycastHit, raycastRange, layersToCheck);

        if (didWeHit)
        {
            Debug.Log("Player left clicked, and hit a spot on the terrain!  Location: " + raycastHit.point.ToString() );
            avatar.CommandMove(raycastHit.point);
        }
        else
        {
            Debug.Log("Selected location wasn't on the terrain.  Can't move there.");
        }


    }

    /// <summary>
    /// This process checks to see if the player entered any input during this turn; IE:  did they click the mouse,
    /// or press a key on the keyboard.
    /// </summary>
    private void ReadInput()
    {
        //Select - by drawing a ray from the camera to whatever it hits first.
        //Valid options:  Terrain, unit.
        bool leftClick = Input.GetButtonDown("LeftClick");
        bool rightClick = Input.GetButtonDown("RightClick");
        bool jump = Input.GetButtonDown("Jump");
        bool escape = Input.GetButtonDown("Cancel");
        bool submit = Input.GetButtonDown("Submit");
        bool gather = Input.GetButtonDown("Gather");

        float lookMovement = Input.GetAxis("LookLeftRight");

        //"SELECT ACTION"
        //Select the first "unit" that you hit with a raycast, shot from the main camera to the position of the mouse cursor.
        //This should happen first.  Even though it's unlikely, the select action should happen before any other command.
        if (leftClick)
        {
            //Draw a ray from the mouse point, to the terrain.
            bool didWeHit;
            Ray mouseRay = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit raycastHit;
            float raycastRange = 300.0f;
            int layersToCheck = LayerMask.GetMask(  "PlayerUnits", 
                                                    "Enemy1Units", 
                                                    "Enemy2Units",
                                                    "Enemy3Units",
                                                    "NeutralUnits");
            didWeHit = Physics.Raycast(mouseRay, out raycastHit, raycastRange, layersToCheck);

            if (didWeHit)
            {
                Debug.Log("Player left clicked, and found a unit!  Selected Unit: ", raycastHit.collider.gameObject);

                //Then, "select" the unit that we hit.
                SelectedObject = raycastHit.collider.gameObject;

                //If the selectedObject is a unit, then...
                if ( SelectedObject.GetComponent<Unit>() )
                {
                    //Tell the unit to mark itself as selected.
                    SelectedObject.GetComponent<Unit>().IsSelected = true;

                }

            }
            else
            {
                
                Debug.Log("Player tried to select; no unit found.");
            }
        }

        if (rightClick)
        {
            ReadMovement();
        }

        //Did the user hit the gathering key?
        if (gather)
        {
            //If so, is there currently an object selected?
            if (SelectedObject)
            {
                //Then tell that selected cultist to go gather!
                Unit selectedCultist = SelectedObject.GetComponent<Unit>();
                selectedCultist.ReceiveCommand(ECommand.Gather);
            }
            else
            {
                Debug.Log("You must have selected a unit, to assign the gather order.");
            }
        }

        //Move pan the camera left or right based off of the "LookLeftRight" controller axis.
        if (lookMovement > .025f || lookMovement < -0.025f)
        {
            camera.transform.Translate(new Vector3(lookMovement, 0.0f, 0.0f));
        }
    }

    /// <summary>
    /// Select a cultist, so that the options for it appear on the UI.
    /// </summary>
    /// <param name="newSelection">Who are we selecting.</param>
    private void Select(GameObject newSelection)
    {
        SelectedObject = newSelection;
    }

    /// <summary>
    /// The interaction will always be context dependent.
    /// If there is no _selectedObject, then this will do nothing!
    /// </summary>
    private void Interact()
    {
        if (_selectedObject)
        {
            Debug.Log("You at least got this far.", _selectedObject);
        }
    }
}

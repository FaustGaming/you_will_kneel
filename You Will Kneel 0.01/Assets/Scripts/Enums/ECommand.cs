﻿/// <summary>
/// The available types of Commands.
/// </summary>
public enum ECommand
{
    //The "Default" value is the one that is first in the list.
    NotSet, 
    Gather,
    GoHome,
    Attack
}

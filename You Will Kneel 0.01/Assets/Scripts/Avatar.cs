﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// This is the monobehaviour for the GameObject representing what is the player character.  
/// The player character gameobject will be referred to as an "Avatar".
/// </summary>
public class Avatar : MonoBehaviour
{
    private NavMeshAgent navMeshAgent;

    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Give the Avatar a move order, telling them where to go.
    /// </summary>
    /// <param name="destination"></param>
    /// <returns>A bool, indicate if or if not you provided a valid destination.</returns>
    public bool CommandMove(Vector3 destination)
    {
        bool result = navMeshAgent.SetDestination(destination);
        return result;
    }
}

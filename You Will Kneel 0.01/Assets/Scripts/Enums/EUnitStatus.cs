﻿public enum EUnitStatus
{
    Resting, //Vulnerable to visions, possession.
    Foraging, //Tends to splinter off into individual gathering parties.  Completely vulnerable, but inducing a vision will not be as effective.
    Hunting, //Tends to splinter off into armed parties.  Not as vulnerable, as they're grouped and armed.  However, a success will have ripple effects on the tribe.
    Fleeing,    //The "This is crazy, I'm out" status.  They're just trying to get out of the woods, as quickly as possible.
    Rallying    //They are building resilience.  Gathering determination to fight back.
}


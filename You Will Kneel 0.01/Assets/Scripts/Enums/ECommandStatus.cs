﻿
    public enum ECommandStatus
    {
        Waiting,    //Signals that this command is waiting to start.  Usful when looking at the queue of commands.
        StartUp,    //The first frame that this command has started.
        Running,    //Signals that this command is currently running.  This is important when examining a queue of commands, to ID which ones are waiting.
        Stopping,   //Winding down.
        Done,       //Signals start of "Exit" phase.
        Error       //No good, can not complete command, signifies start of error handling.
    }

